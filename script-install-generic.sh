#!/bin/bash

# ----
echo "  -> Installation Gimp"
sudo apt install gimp -y

# ----
echo "  -> Installation keepassxc"
sudo add-apt-repository ppa:phoerious/keepassxc
sudo apt-get update
sudo apt-get install keepassxc -y

# ----
echo "  -> Installation git"
sudo apt-get install git -y

# ----
echo "  -> Installation inkscape"
sudo add-apt-repository ppa:inkscape.dev/stable-0.92 -y
sudo apt update
sudo apt install inkscape -y
sudo chmod a+rwx .config/inkscape/

# ----
echo "  -> Installation laserengraver"
cd /home/$USER/Téléchargements/
wget https://gitlab.com/PlateformeC/installation-pingouin/-/raw/master/installation-laserengraver.sh
sudo sh installation-laserengraver.sh

# ----
echo "  -> Installation inkcut"
sudo apt -y install python3-pip python3-pyqt5 python3-pyqt5.qtsvg libcups2-dev

cd /home/$USER/Téléchargements/

wget https://inkscape.org/gallery/item/12796/inkcut-for-inkscape-2.1.1.tar.gz

tar -xzvf inkcut-for-inkscape-2.1.1.tar.gz

sudo mkdir -p /home/$USER/.config/inkscape/extensions
sudo cp /home/$USER/Téléchargements/inkscape/inkcut* /home/$USER/.config/inkscape/extensions/

# ----
echo "  -> Config dock gnome"
gsettings set org.gnome.shell favorite-apps "['firefox.desktop', 'thunderbird.desktop', 'nautilus.desktop', 'inkscape.desktop']"

# ----
echo "  -> Nettoyage"
sudo rm -r /home/$USER/Téléchargements/*

